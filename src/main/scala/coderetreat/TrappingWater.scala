package coderetreat

/**
  * Created by andrejs.dasko on 18-Nov-17.
  */

object TrappingWater {

  def trim(input: List[Int]) = input.slice(input.indexOf(1), input.lastIndexOf(1) + 1)

  def getBottom(input: List[Int]): List[Int] = input.map(p => if (p > 0) 1 else 0)

  def waterAtBottom(input: List[Int]): Int = trim(getBottom(input)).count(_ == 0)

  def removeBottom(input: List[Int]) = input.map(p => if (p > 0) p - 1 else p)

  def trappingWater(input: List[Int]): Int = {
    def recursion(input: List[Int], acc: Int): Int = {
      val waterBottom = waterAtBottom(input)
      if (waterBottom == 0) acc
      else recursion(removeBottom(input), waterBottom + acc)
    }
    recursion(input, 0)
  }
}
