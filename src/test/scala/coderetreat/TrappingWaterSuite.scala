package coderetreat

import org.scalatest.FunSuite

class TrappingWaterSuite extends FunSuite {

  test("No trim required") {
    val actual = TrappingWater.trappingWater(List())
    val expected = 0
    assert(expected == actual)
  }

  test("Left trim") {
    val actual = TrappingWater.trim(List(0, 1, 0, 1))
    val expected = List(1, 0, 1)
    assert(expected == actual)
  }

  test("Right trim") {
    val actual = TrappingWater.trim(List(1, 0, 1, 0))
    val expected = List(1, 0, 1)
    assert(expected == actual)
  }

  test("Full trim") {
    val actual = TrappingWater.trim(List(0, 0, 1, 0, 1, 0, 0))
    val expected = List(1, 0, 1)
    assert(expected == actual)
  }

  test("getBottom") {
    val actual = TrappingWater.getBottom(List(1, 0, 2))
    val expected = List(1, 0, 1)
    assert(expected == actual)
  }

  test("removeBottom") {
    val actual = TrappingWater.removeBottom(List(1, 2, 2))
    val expected = List(0, 1, 1)
    assert(expected == actual)
  }

  test("removeBottom: remove last hill") {
    val actual = TrappingWater.removeBottom(List(0, 0, 1))
    val expected = List(0, 0, 0)
    assert(expected == actual)
  }

  test("removeBottom: remove bottom from empty plain input") {
    val actual = TrappingWater.removeBottom(List(0, 0, 0))
    val expected = List(0, 0, 0)
    assert(expected == actual)
  }

  test("trappingWater") {
    val actual = TrappingWater.trappingWater(List(0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1))
    val expected = 6
    assert(expected == actual)
  }

  test("trappingWater: hills of the same height") {
    val actual = TrappingWater.trappingWater(List(0, 1, 0, 2, 1, 0, 1, 2, 2, 1, 2, 1))
    val expected = 6
    assert(expected == actual)
  }
}
