# Code retreat 2017-Nov-18

Final iteration of the trapping rain water problem as solved during the Global day of Code Retreat 2017.	
Problem described here: https://www.geeksforgeeks.org/trapping-rain-water.	
Solution: [TrappingWater.scala](src/main/scala/coderetreat/TrappingWater.scala)